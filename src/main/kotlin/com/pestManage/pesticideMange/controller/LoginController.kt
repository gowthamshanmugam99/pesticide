package com.pestManage.pesticideMange.controller

import com.pestManage.pesticideMange.exception.GetException
import com.pestManage.pesticideMange.model.*
import com.pestManage.pesticideMange.service.*
import com.pestManage.pesticideMange.util.JwtUtils
import org.springframework.web.bind.annotation.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

@RestController
@CrossOrigin(origins = ["http://localhost:3000"], allowCredentials = "true")
class LoginController(
    var loginService: LoginService,
    var farmerService: FarmerService,
    var adminService: AdminService,
    var consultantService: ConsultantService,
    var warehouseOperatorService: WarehouseOperatorService,
    var deliverPersonService: DeliverPersonService,
    var util: JwtUtils

) {
    private  val jwtNull = "Email and password Not Found"
    private val invalidType = "Inavlid type"

    @PutMapping("/login")
    fun login(@RequestBody loginDTO: LoginDTO, response: HttpServletResponse): Any {
        return this.loginService.login(loginDTO.email, loginDTO.password, response)
    }

    @GetMapping("/responsefarmer")
    fun user(@CookieValue("Farmer") jwt: String?): Farmer {
        try {
            if (jwt == null) {
                throw GetException(jwtNull)
            }
            val body = util.verify(jwt)
            if (body["type"]?.equals("Farmer") == true) {
                return this.farmerService.getFarmerById(body.issuer)
            }

            else{
                throw GetException(invalidType)
            }
        } catch (e: Exception) {
            throw GetException("unauthenticated")
        }
    }
    @GetMapping("/responseconsultant")
    fun consultant(@CookieValue("Consultant") jwt: String?): Consultant {
        try {
            if (jwt == null) {
                throw GetException(jwtNull)
            }
            val body = util.verify(jwt)
          if (body["type"]?.equals("Consultant")== true) {
               return this.consultantService.viewConsultant(body.issuer)
            }
            else{
               throw GetException(invalidType)
            }
        } catch (e: Exception) {
            throw GetException("unauthenticated")
        }
    }
    @GetMapping("/responseadmin")
    fun admin(@CookieValue("Admin") jwt: String?): Admin {
        try {
            if (jwt == null) {
                throw GetException(jwtNull)
            }
            val body = util.verify(jwt)
           if (body["type"]?.equals("Admin")== true) {
                return this.adminService.viewAdmin(body.issuer.toString())
            }
            else{
                throw GetException(invalidType)
            }
        } catch (e: Exception) {
            throw GetException("unauthenticated")
        }
    }
    @GetMapping("/responsewarehouseoperator")
    fun warehouseoperator(@CookieValue("WarehouseOperator") jwt: String?): WarehouseOperator {
        try {
            if (jwt == null) {
                throw GetException(jwtNull)
            }
            val body = util.verify(jwt)
            if (body["type"]?.equals("WarehouseOperator")== true) {
              return this.warehouseOperatorService.viewWarehouseOperator(body.issuer)
            }
            else{
               throw GetException(invalidType)
            }
        } catch (e: Exception) {
           throw GetException("unauthenticated")
        }
    }

    @GetMapping("/responsedeliverperson")
    fun deliverperson(@CookieValue("DeliverPerson") jwt: String?): Delivery {
        try {
            if (jwt == null) {
               throw GetException(jwtNull)
            }
            val body = util.verify(jwt)
             if (body["type"]?.equals("DeliverPerson")== true) {
                return this.deliverPersonService.viewByEmail(body.issuer)
            }
            else{
                throw GetException(invalidType)
            }
        } catch (e: Exception) {
           throw GetException("unauthenticated")
        }
    }
    @PutMapping("/logout")
    fun logout(@RequestBody logoutDTO: LogoutDTO, response: HttpServletResponse): String {

        val cookie = Cookie(logoutDTO.type, "")
        cookie.maxAge = 0
        response.addCookie(cookie)

        return this.loginService.logout(logoutDTO.email)
    }

    @GetMapping("/forgetpassword/{email}")
    fun forgetPassword(@PathVariable email: String): String {
        return this.loginService.forgetPassword(email)
    }
}