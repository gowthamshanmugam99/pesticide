package com.pestManage.pesticideMange

import com.pestManage.pesticideMange.controller.DiseaseController
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.runApplication
import java.io.File

@SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
class PesticideMangeApplication

fun main(args: Array<String>) {
	//File(DiseaseController.uploadDirectory).mkdir()
	runApplication<PesticideMangeApplication>(*args)
}
